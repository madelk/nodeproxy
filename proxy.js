'use strict';

var express = require('express');
var app = express();
var domain = process.argv[2];
var request = require("request");
var fs = require('fs');
var cheerio = require('cheerio');
var url = require('url');
var querystring
    = require('querystring')
var port = 8000;
var Entities = require('html-entities').XmlEntities;

if (domain == null) {
    console.log('specify a domain');
    return;
}
app.use('/', express.static('content'));
app.use(function (req, res, next) {
    var requestedurl = url.parse(req.originalUrl, true, true);
    if (!requestedurl.host) requestedurl = url.parse("http://" + domain + req.originalUrl, true, true);
    if (requestedurl.query.nodeproxyoriginalhost) {
        requestedurl.host = requestedurl.query.nodeproxyoriginalhost;
        requestedurl.hostname = requestedurl.query.nodeproxyoriginalhost;
    };

    request({
        uri: requestedurl,
        encoding: null
    }, function (error, response, body) {


        var $ = cheerio.load(body);

        var hadToParse = false;

        var stylesheets = $('link[rel="stylesheet"]');
        for (var i = 0; i < stylesheets.length; i++) {
            var value = stylesheets[i];
            var parsedurl = url.parse(value.attribs.href, false, true);
            var querys = querystring.parse(parsedurl.query);
            querys.nodeproxyoriginalhost = parsedurl.host;
            parsedurl.host = 'localhost:' + port;
            parsedurl.search = querystring.stringify(querys);
            value.attribs.href = url.format(parsedurl);
            hadToParse = true;
        }
        var images = $('img');
        for (var i = 0; i < images.length; i++) {
            var value = images[i];
            if (!value.attribs.src) continue;
            var parsedurl = url.parse(value.attribs.src, false, true);
            var querys = querystring.parse(parsedurl.query);
            querys.nodeproxyoriginalhost = parsedurl.host;
            parsedurl.host = 'localhost:' + port;
            parsedurl.search = querystring.stringify(querys);
            value.attribs.src = url.format(parsedurl);
            hadToParse = true;
        }

        var javascript = $('script');
        for (var i = 0; i < javascript.length; i++) {
            var value = javascript[i];
            if (!value.attribs.src) continue;
            var parsedurl = url.parse(value.attribs.src, false, true);
            var querys = querystring.parse(parsedurl.query);
            querys.nodeproxyoriginalhost = parsedurl.host;
            parsedurl.host = 'localhost:' + port;
            parsedurl.search = querystring.stringify(querys);
            value.attribs.src = url.format(parsedurl);
            hadToParse = true;
        }

        res.headers = response.headers;
        //     if (response.request.href.includes('open-sans-regular-bold')){
        //         var hello = 'hi';
        //     }
        res.setHeader('content-type', response.headers["content-type"]);
        if (hadToParse) {
            res.send(Entities.decode($.html()));
        }
        else {
            res.end(body);
        }
    });
});

app.use(express.static('content' + "/" + domain));

app.listen(port, function () {
    console.log('Proxy running on port ' + port);
});